# Ansible roles and playbooks for salsa.debian.org

This ansible repos should contain everything you need
to maintain gitlab on salsa.debian.org.

We use several playbooks, most of them have a very
specific purpose and should not been run without extra care.

## Run Ansible

Due to restrictions of ansible we have to run ansible on godard itself
as user `git`. Please make sure that you have bash as running shell.
There is a checkout of this repository on ~/ansible. Please don't do any
local modifications of the ansible files.

## Playbooks

### install.yml

This playbooks installs or upgrades gitlab. Ensure that puppet ran and
that the files ~/.credentials-manual.yaml and ~/.credentials.yaml exist.

**ATTENTION** Don't set any of those parameters unless you are really sure.

Initialization of some parts are controlled by parameters, defaulting to false.
You have to give them to ansible from the commandline, like:
    ansible-playbook playbooks/install.yml -e 'init_database=true'

The following parameters are supported:

- `init_database` - drop database and reinitialize
- `go_install_force` - Force re-installation of all Go applications

### configure.yml

This playbook installs all configuration files and restarts
services if needed. It doesn't support any parameters and should be safe to run at every time.

### system.yml

This playbook creates a test system that tries to match the systems created by DSA to an extend.
It installs and configures the following software needed for the GitLab setup:

* PostgreSQL and database for GitLab.
* Redis.
* Postfix with local-only delivery to a Dovecot and some Mutt setup to read the mails.
* Nginx as HTTP proxy with dehydrated for certificate setup.

## Upgrading

Basic step for upgrading Gitlab using this Ansible stuff.

* Make sure our [Gitlab fork](https://salsa.debian.org/salsa/gitlab-ce) is up-to-date and have a release tag. Our modifications are in a branch `$major-$minor-stable-salsa`, upstream releases are in `$major-$minor-stable`.
* Update versions in `playbooks/group_vars/all.yml` for all components.
* Run `playbooks/install.yml`.

## Variables

The playbooks define some variables, which needs to be modified as needed.

* `gitlab_repo` - URL to the Gitlab repo, as salsa runs a slightly patched one.
* `gitlab_version` - Release tag for Gitlab.
* `gitlab_pages_version` - Release tag for Gitlab Pages. Retrieve from `GITLAB_PAGES_VERSION` file.
* `gitlab_shell_version` - Release tag for Gitlab Shell. Retrieve from `GITLAB_SHELL_VERSION` file.
* `gitlab_workhorse_version` - Release tag for Gitlab Workhorse. Retrieve from `GITLAB_WORKHORSE_VERSION` file.
* `gitaly_version` - Release tag for Gitaly. Retrieve from `GITALY_SERVER_VERSION` file.
