---
- name: stop services
  systemd:
    name: "{{ item }}"
    state: stopped
    scope: user
  with_items:
    - gitlab-mailroom
    - gitlab-sidekiq

- name: check database
  command: >
    {{ ruby_bundle }} exec rails runner -e production "exit 2 unless ActiveRecord::SchemaMigration.table_exists?"
  args:
    chdir: "{{ gitlab_home }}"
  changed_when: false
  register: db_status
  failed_when: "db_status.rc not in (0, 2)"

- name: check empty database
  fail:
    msg: "Please set parameter 'init_database=true' if you really want to initialize the database"
  when: "not ansible_check_mode and not init_database | default(false) and db_status.rc == 2"

- name: init gitlab database
  shell: >
    {{ ruby_bundle }} exec rake db:schema:load db:seed_fu RAILS_ENV=production
  args:
    chdir: "{{ gitlab_home }}"
  when: "not ansible_check_mode and db_status.rc == 2 and init_database | default(false)"

- name: create backup
  command: >
    {{ ruby_bundle }} exec rake gitlab:backup:create RAILS_ENV=production SKIP=artifacts,builds,lfs,pages,registry,repositories,uploads
  args:
    chdir: "{{ gitlab_home }}"
  when: "not ansible_check_mode and db_status.rc == 0 and backup | default(true)"

- name: run database migrations
  command: >
    {{ ruby_bundle }} exec rake db:migrate RAILS_ENV=production SKIP_POST_DEPLOYMENT_MIGRATIONS=true
  args:
    chdir: "{{ gitlab_home }}"
  notify:
    - restart gitlab-mailroom
    - restart gitlab-sidekiq
    - reload gitlab-unicorn
